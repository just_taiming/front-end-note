# CSS Transitions 轉場效果

## 簡述

CSS transitions allows you to change property values smoothly, over a given duration.

允許您在給定的「時間內」、「平穩地」更改「屬性值」。

## 使用姿勢

- **屬性值(transition-property)**
  - 定義哪些CSS屬性會被轉場效果影響。
  - ex: background, color, width/height, opacity, transform, left, right...
  - transition 有兩個不作用的元素（display，visibility）
- **轉場持續時間(transition-duration)**
  - 定義轉場的持續時間。時間通常以s為單位(秒)，可以定義小數點例如0.5s或.5s，預設值是0s。
  - 單位為s或ms
- **延遲多久轉換(transition-delay)**
  - 單位為s或ms
- **時間函式(transition-timing-function)**
  - 轉換時的速度曲線，用來設定轉場過程時所使用的貝茲曲線。
  - ex:
    - linear (均速)
    - ease (緩入、中間快、緩出，預設值)
    - ease-in (緩入)
    - ease-out (緩出)
    - ease-in-out (緩入緩出，對比於ease較平緩)
    - cubic-bezier(n,n,n,n) (利用貝茲曲線自定義速度模式，n=0~1中的數值)
  - [transition-timing-function](https://developer.mozilla.org/en-US/docs/Web/CSS/transition-timing-function)

![timing function](https://ithelp.ithome.com.tw/upload/images/20181017/20111500dECPJzMVeo.png)

```css
transition: transition-property | transition-duration | transition-timing-function | transition-delay;
```

如果要雙向動畫都相同，只要在原始狀態加入transition屬性。
ex: https://www.w3schools.com/css/tryit.asp?filename=trycss3_transition1

## 適合使用 Transition 的時機/無法使用的情況/限制

- **transition 必須是「確切數值」的轉換**
  - transition 轉換的開始和結束屬性值都必須是確切數值，否則將無法進行計算
  - 不適合使用的範例 ex:
    - width: auto(不確定的值)轉換成width:100px(確切值)。
    - display: none 轉換成 display:block，none 和 block 都不是數值。
- **transition 需要事件觸發**
  - 沒辦法一進頁面自動產生效果。需透過JavaScript事件處理或是就只能配合與事件有關的偽類別(:hover、:focus 等)呈現效果。

## Teaches 專案用到的地方

- animation tab
- arrow button of expansion panel

## Reference

https://eyesofkids.gitbooks.io/css3/content/contents/transitions.html
https://www.w3schools.com/css/css3_transitions.asp
