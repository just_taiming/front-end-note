# Animation/Keyframe 關鍵影格

## 簡述

動畫的這三個屬性，持續期間、時間函式與延遲開始時間，這部份的內容與 CSS3 轉場一模一樣。

## 使用姿勢

- **自定義動畫名稱(animation-name)**
  - 與 transition-property 相對應，不同在於 transition-property 是指定 css 屬性，animation-name 指定預先定義好的動畫名稱。
- **持續期間(animation-duration)**
  - 時間通常以s為單位(秒)，可以定義小數點例如 0.5s 或 .5s ，預設值是0s。
- **時間函式(animation-timing-function)**
  - 這是用來設定轉場過程時所使用的貝茲曲線，預設值是 ease。
- **延遲開始時間(animation-delay)**
  - 時間通常以s為單位(秒)，可以定義小數點例如 0.5s 或 .5s ，預設值是0s。

## 與 CSS Transition 的比較

### 相似之處

擁有相似作用的欄位

| 項目         | 作用屬性/動畫名稱           | 持續期間                | 延遲多久             | 時間函式/曲線                    |
| ---------- | ------------------- | ------------------- | ---------------- | -------------------------- |
| transition | transition-property | transition-duration | transition-delay | transition-timing-function |
| animation  | animation-name      | animation-duration  | animation-delay  | animation-timing-function  |

### 相異之處

1. 「不一定」要綁定事件才能觸發(ex: hover, focus, active, blur......)。
2. Animation 獨有的屬性：

- **播放循環次數(animation-iteration-count)**
  - 預設是1，可以設定正整數，例如 5。
  - 可以是帶有小數點的小數，例如 0.5 代表播放半次。
  - infinite 代表永不停止的播放。
- **播放方向(animation-direction)**
  - 用於定義播放的方向
    - ex:
      - normal (正常)
      - reverse (反向/倒帶)
      - alternate (輪流)
      - alternate-reverse (輪流與反向)
- **播放後樣式(animation-fill-mode)**
  - 指的是動畫播放後的樣式，是不是要保留在最後的狀態
    - ex:
      - none (預設)
      - forwards (停留在最後的樣式)
      - backwards (回到一開始的樣式)
- **播放狀態(animation-play-state)**
  - 這個屬性可以控制動畫的播放和暫停
    - ex:
      - running (預設)
      - paused

定義動畫的關鍵影格

```css
@keyframes pinpon {
  0% {
    left: 0;
    top: 0;
  }
  50% {
    left: 200px;
    top: 150px;
  }
  100% {
    left: 300px;
    top: 0;
  }
}
```

定義動畫的屬性範例：

```css
.some-class-name {
   animation-name: pinpon;
   animation-duration: 0s;
   animation-timing-function: ease;
   animation-delay: 0s;
   animation-iteration-count: 1;
   animation-direction: normal;
   animation-fill-mode: none;
   animation-play-state: running;
}
```

## 適合使用 Animation/Keyframe 的時機

- 「不一定」要綁定事件才能觸發 (ex: hover, focus, active, blur......)。
- 適合用來做較細微的動畫表現。需要明確的指定關鍵影格(@keyframes)的參數。
- 網頁加載時會直接執行，可以自行控制各階段動畫的變化

## Teaches 專案用到的地方

- FadeIn, FadeOut 效果

## Reference

https://eyesofkids.gitbooks.io/css3/content/contents/keyframe.html
