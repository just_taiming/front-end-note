# React Transition Group

## 簡述

幫助我們處理元件從進場到退場各個狀態更細節的表現。

1. 可以讓我們控制從進場到退場各個狀態時，可以執行什麼動作或觸發什麼事件。
2. 在各個狀態幫我們加上 class name ，幫助我們控制其樣式。

## 事件處理

- Enter
  - onEnter 開始要要進場
  - onEntering 正在進場中
  - onEntered 完成進場
- Exit
  - onExit 開始要退場
  - onExiting 正在退場中
  - onExited 完成退場

## 樣式處理

- namespace-enter: 進入前的樣式或是稱作初始值
- namespace-enter-active: 進入動畫直到完成時之前的 CSS 樣式;
- namespace-enter-done: 進入完成時的 CSS 樣式;
- namespace-exit: 退出前的 CSS 樣式;
- namespace-exit-active: 退出動畫知道完成時之前的的 CSS 樣式。
- namespace-exit-done: 退出完成時的 CSS 樣式。

```css
.some-class-name-enter {
    opacity: 0;
}
.some-class-name-enter-active {
    opacity: 1;
    transition: opacity 2000ms;
}
.some-class-name-enter-done {
    opacity: 1;
}


.some-class-name-exit {
    opacity: 1;
}
.some-class-name-exit-active {
    opacity: 0;
    transition: opacity 2000ms;
}
.some-class-name-exit-done {
    opacity: 0;
}
```

## Teaches 專案用到的地方

- Custom Modal 彈窗
- Carousel 名人教師畫廊

## Reference

http://reactcommunity.org/react-transition-group/
