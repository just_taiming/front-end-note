# GitBook Plugins

我們有時候會羨慕別人的 GitBook 怎麼比較華麗，和我們不太一樣？又可以留言、側邊欄可以收合、側邊欄可以調整寬度、有懸浮按鈕可以回到頂端......等等，可以做一些特別炫砲的事情。

不用擔心，我們裝一些插件，一樣也可以做得到。

這邊我們以目錄標題可以收合的插件 `gitbook-plugin-chapter-fold` 來做個簡單的示範。

## 新增 book.json 目錄

book.json 這個檔案在幫助我們做 GitBook 的套件管理，他扮演的角色有點類似 Node.js 應用程式下面的 `package.json`。

所以第一步，我們需要在根目錄下新增一個 book.json 檔案，並且配置我們想要安裝的套件，如圖：
![](images/book-json.png)

當然有可能我們在網路上搜尋其他資料的時候，會發現人家的 book.json 長得很複雜，因為有可能其他的套件需要一些更複雜的配置，這邊會因著安裝不同的套件而不同，所以安裝套件前，需要先看一下使用說明。說明的部分可以上 npm 的網站上去看，這個插件的連結我也附在下面。

## 安裝

安裝方式非常簡單，我們在配置好 book.json 之後，只要在根目錄下執行 `gitbook install` 就可以了！

![](images/gitbook-install.png)

注意，安裝完如果沒有任何動靜，有可能是你沒有重新啟動 gitbook。

## Chapter fold 配置注意事項

有時候並不是安裝好套件就沒事了，我們需要仔細瞭解這個套件的配置以及使用方式。

以 `gitbook-plugin-chapter-fold` 這個套件為例，我們需要注意 `SUMMARY.md` 的格式，如果格式不符合，就無法達到我們想要的收合效果，因此，想要收合目錄， `SUMMARY.md` 的格式應該如下：

```md
* [GitBook](GitBook/index.md)
  * [如何在 Gitlab 平台簡單創建 GitBook 電子書](Gitbook/how-to-install-gitbook.md)
  * [GitBook Plugins](Gitbook/gitbook-plugins.md)
```

## Reference

- [gitbook常用的插件](https://segmentfault.com/a/1190000019806829)
- [Chapter fold for GitBook](https://www.npmjs.com/package/gitbook-plugin-chapter-fold)
- [GitBook插件整理](https://www.jianshu.com/p/427b8bb066e6)
- [GitBook 插件](http://gitbook.zhangjikai.com/plugins.html)
