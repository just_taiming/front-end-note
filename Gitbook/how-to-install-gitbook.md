# 如何在 Gitlab 平台簡單創建 GitBook 電子書

## 在 Gitlab 上新增空白專案

其實還蠻直白的，如標題所述，就是新增一個 Gitlab 專案，下面列出幾個要注意的地方：

1. 之後預設網址為 `https://username.gitlab.io/project-name`，所以取的 project name 會影響到之後的網址，記得好好取個名字。
2. Visibility Level 記得要選 「Public」，如下圖標示。
3. `Initialize repository with README` 記得勾起來，因為對 GitBook 而言，「README.md」是一個必要的檔案。

![](http://images.kenming.idv.tw/2018/season3/create-gitbook-project-at-gitlab-01.png)

## 設定 Gitlab CI/CD

我們在編輯 GitBook 一直到發佈到線上，過程中需要經過許多的步驟，這些步驟非常繁瑣，而且很多都是重複的動作、重複的指令。因此我們希望把這些每次要發布 GitBook 都會需要做的重複性動作交給電腦，我們只需要專注在編輯 GitBook 本身就好了。

這邊需要在專案的根目錄新增一個「.gitlab-ci.yml」檔案，可以把專案整個 git pull 下來之後新增，也可以直接在 Web UI 上面新增，若要透過 Web UI 新增，則在專案的主頁下，點選「CI/CD」按鈕，然後把下面這一段內容一模一樣貼上

[.gitlab-ci.yml](https://gitlab.com/pages/gitbook/blob/master/.gitlab-ci.yml)
```yml
# requiring the environment of NodeJS 10
image: node:10

# add 'node_modules' to cache for speeding up builds
cache:
  paths:
    - node_modules/ # Node modules and dependencies

before_script:
  - npm install gitbook-cli -g # install gitbook
  - gitbook fetch 3.2.3 # fetch final stable version
  - gitbook install # add any requested plugins in book.json

test:
  stage: test
  script:
    - gitbook build . public # build to public path
  only:
    - branches # this job will affect every branch except 'master'
  except:
    - master

# the 'pages' job will deploy and build your site to the 'public' path
pages:
  stage: deploy
  script:
    - gitbook build . public # build to public path
  artifacts:
    paths:
      - public
    expire_in: 1 week
  only:
    - master # this job will affect only the 'master' branch
```

在 Web UI 上面貼上的示意圖：

![](http://images.kenming.idv.tw/2018/season3/create-gitbook-project-at-gitlab-02.png)

按下 `Commit Changes` 按鈕之後， Gitlab 就會根據你的 `.gitlab-ci.yml` 檔案去執行 CI/CD Pipeline，如下

![](images/cicd-pipeline.png)

## 新增檔案 SUMMARY.md

「SUMMARY.md」對 GitBook 來說也是一個必要檔案，他會決定文件目錄的結構。意思就是說， GitBook 架起來之後，左邊會有一個 Side Menu，那個 Side Menu 的結構會由這個檔案來決定。因此我們需要新增一個「SUMMARY.md」檔案到根目錄。

我們可以先簡單加入下列內容：

```md
# Summary

* [Introduction](README.md)
```

中括號(或是說方括號)裡面是 `title`，也就是顯示出來的文字，小括號裡面，是文件的`路徑`，也就是說，你的`檔案階層架構`其實是可以跟你的`目錄階層架構`不一致。

如果你需要多層的目錄階層架構，可以模仿下面範例用縮排的方式表示階層，若你需要的不只兩層，可能三層、四層的結構，你也可以透過縮排來達到：

```md
# Summary

* [卷 I](part1/README.md)
    * [写作很棒](part1/writing.md)
    * [GitBook很酷](part1/gitbook.md)
* [卷 II](part2/README.md)
    * [期待反馈](part2/feedback_please.md)
    * [更好的写作工具](part2/better_tools.md)
```

## 取得 GitBook 網址

工具列點選「Setting」→「Pages」，就可以察知專案文檔的靜態網頁網址，預設一般為 https://username.gitlab.io/project-name。

![](images/gitbook-path.png)

## 在本地端跑 GitBook

就像我們一般的開發流程，我們需要在本地端測試好，才能部署到產品線。

### 安裝 Node.js

在安裝 GitBook 必須先裝 Node.js ， Node.js 是一個讓 Javascript 運行在服務端的開發平台。
安裝方式我放在[這邊](https://wcc723.github.io/design/2014/09/04/gitbook-install/)

### 安裝 gitbook

```shell
npm install -g gitbook
```

如果你安裝成功，就能夠察看他的版本

```shell
gitbook version
```

最後，在你的根目錄，輸入

```shell
gitbook serve
```

它就會打開一個 port，並且運行 gitbook server

![](images/gitbook-serve.png)

### 增加 .gitignore

如果你有使用 git 的話(因為我們是用 Gitlab ，所以基本上應該會用上 git )，特別是如果像我一樣使用 VScode  並且搭配 git extension 來編輯，就會發現跑完 `gitbook server` 之後，在你的根目錄下多了一些非你手動操作的變更，最主要的其實就是多了 `_book` 這個資料夾及其下的內容，因為這邊是 gitbook server 它自動幫我們產生的，其實我們不需要對這個檔案做版本控制，不希望他進 git ，所以我們要透過在根目錄新增一個 `.gitignore` 檔案，並且在其中做一些設定，來忽略這些檔案， `.gitignore` 內容參考如下：

```.gitignore
/_book/
/node_modules/
/book.pdf
/book.mobi
/book.epub
/public
```

## Reference

- [在 Gitlab 平台簡單創建 GitBook 電子書的步驟](https://www.kenming.idv.tw/simple-create-gitbook_at_gitlab_steps/)
- [安裝Gitbook | 卡斯伯Blog - 前端，沒有極限](https://wcc723.github.io/design/2014/09/04/gitbook-install/)
- [不懂 Git 也能善用 GitBook](https://wastemobile.gitbooks.io/gitbook-chinese/content/book/nogitisok.html)
